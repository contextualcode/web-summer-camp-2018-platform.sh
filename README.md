# Setup GIT remotes
## Platform.sh remote
```bash
cd /home/websc/www/ez/cloud
git clone git@gitlab.com:<USERNAME>/web-summer-camp-2018-platform.sh.git
cd web-summer-camp-2018-platform.sh
git remote add platform <PLATFORMSH-REPO_URI>
git remote -v
```
## ContextualCode remote
```bash
git remote add contextualcode git@gitlab.com:contextualcode/web-summer-camp-2018-platform.sh.git
git fetch contextualcode --tags
git remote -v
```
## Useful links
- [Platform.sh GitLab Integration](https://docs.platform.sh/administration/integrations/gitlab.html)
- [Platform.sh GitHub Integration](https://docs.platform.sh/administration/integrations/github.html)
- [Platform.sh Bitbucket Integration](https://docs.platform.sh/administration/integrations/bitbucket.html)

---------------

# First Platform.sh commit
## Pushing code changes
```bash
git checkout -b master
git merge tags/initial
git push -u origin master
git push platform master
```

---------------

# Platform.sh Command Line Interface
## Installation
```bash
curl -sS https://platform.sh/cli/installer | php
platform bot --parrot --party
```
## Authentication
```bash
platform auth:browser-login
platform
```
## Usage
```bash
platform list
platform ssh --project=<PLATFORMSH_PROJECT_ID> --environment=master
```
## Useful links
- [Platform.sh CLI](https://docs.platform.sh/gettingstarted/cli.html)

---------------

# Disk mounts
## Basics
```bash
git merge tags/disk_mounts_basics
git push origin master && git push platform master
platform ssh
df -ha | grep logs
tail /app/logs/access.log
```
## Public
```bash
git merge tags/disk_mounts_public
git push origin master && git push platform master
platform ssh
df -ha | grep uploads
echo "Download me" > /app/web/uploads/download.txt
```
## Useful links
- [Mounts](https://docs.platform.sh/configuration/app/storage.html#mounts)
- [Low-disk warning](https://docs.platform.sh/administration/integrations/notifications.html#low-disk-warning)

---------------

# Services
## Database service
```bash
git merge tags/database_service
git push origin master && git push platform master
```
## Access database inside Platform.sh instance
```bash
platform relationships
platform ssh
mysql -u main -p<PASSWORD> -h db-main.internal
```
## Access database using Platform.sh CLI
```bash
platform db:sql --relationship db-main
CREATE TABLE fruits (name VARCHAR(255) DEFAULT NULL);
INSERT INTO fruits VALUES ("watermelon");
INSERT INTO fruits VALUES ("avocado");
INSERT INTO fruits VALUES ("cucumber");
```
## Access database service in the app
```bash
git merge tags/accessing_database_service
git push origin master && git push platform master
```
## Useful links
- [Configure Services](https://docs.platform.sh/configuration/services.html)
- [MariaDB/MySQL Database service](https://docs.platform.sh/configuration/services/mysql.html#mariadbmysql-database-service)

---------------

# Hooks
## Build hook: install Platform.sh CLI
```bash
git merge tags/build_hook
git push origin master && git push platform master
platform ssh
platform bot --parrot --party
```
## Deploy hook: clear cache
```bash
git merge tags/deploy_hook
git push origin master && git push platform master
platform ssh
tail /var/log/deploy.log
```
## Useful links
- [Hooks](https://docs.platform.sh/configuration/app/build.html#hooks)

---------------

# Variables
## Project variable: Platform.sh CLI access token
```bash
platform variable:create --level project --name 'env:PLATFORMSH_CLI_TOKEN' --value 'SECRET_VALUE' --visible-runtime true --visible-build true --sensitive true --json false
platform redeploy --yes
platform ssh
platform welcome
```
## Application variable
```bash
git merge tags/application_variable
git push origin master && git push platform master
```
## PHP-specific variables
```bash
git merge tags/php_variables
git push origin master && git push platform master
```
## Useful links
- [Variables](https://docs.platform.sh/development/variables.html)
- [CLI API Tokens](https://docs.platform.sh/gettingstarted/cli/api-tokens.html)

---------------

# Cron
## Snapshot cron
```bash
git merge tags/snapshot_cron
git push origin master && git push platform master
platform ssh "tail /var/log/cron.log"
```
## Useful links
- [Cron jobs](https://docs.platform.sh/configuration/app/cron.html#cron-jobs)
